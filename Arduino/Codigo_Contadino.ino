/*
   ---------------------------------------------
   Programa: Esboço do Código Final - Sem Wi-Fi
   Autor: Gabriel Felix
   ---------------------------------------------
*/

#include <SD.h>
#include <DHT.h>
#include <DS1307.h>
#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#include <SPI.h>
#include <ArduinoJson.h>


/*****************    AREA 51   *******************************/
int dStatus, hStatus, mStatus;
int totalDias;
boolean fimMat = false;
String idProd;




int cooler = 12;
int geladeira = 11;
int umidificador = 10;
boolean circulacao = false;
// diaX[Dia, Temperatura, Umidade]

float dAtivo[3], tMedia, uMedia;

// cMillis -> millis compara, sMillis -> millis Status
// bMillis -> millis banco, dMillis -> millis dia

unsigned long cMillis, sMillis, bMillis, dMillis;


/*********************************************************************/


//Construtor do Modulo RTC e declaraçao das variareis usadas nele
DS1307 rtc(A4, A5);
String tempo, hora, minuto, data;

//Declaração do LCD
LiquidCrystal_I2C lcd(0x27, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE);


//Declaração e construtor dos pinos dos sensores DHT
#define DHTTYPE DHT22
#define DHTPIN1 6
#define DHTPIN2 7
DHT dht1(DHTPIN1, DHTTYPE);
DHT dht2(DHTPIN2, DHTTYPE);


//Criação do Byte para mostrar o simbolo de º no LCD
byte grau[8] = {B00110, B01001, B00110, B00000, B00000, B00000, B00000, B00000,};
float u1, t1, u2, t2;

//Constante com o pino CS do Módulo SD
const int CS_PIN = 53;


void setup()
{
 //Start do Serial
  Serial.begin(9600);
  
  pinMode(cooler, OUTPUT);
  pinMode(geladeira, OUTPUT);
  pinMode(umidificador, OUTPUT);

  digitalWrite(cooler, HIGH);
  digitalWrite(geladeira, HIGH);
  digitalWrite(umidificador, HIGH);


  bMillis = sMillis = dMillis = cMillis = millis();

  //Iniciando sensores DHT
  dht1.begin();
  dht2.begin();

  //Iniciando o LCD
  lcd.begin (16, 2);
  lcd.setBacklight(HIGH);
  lcd.createChar(0, grau); //Criando o char º

  rtc.halt(false);
  rtc.setSQWRate(SQW_RATE_1);
  rtc.enableSQW(true);

  tempo = rtc.getTimeStr();//Coletando a String de tempo (HH:MM:SS)
  data = rtc.getDateStr(FORMAT_LONG);

  hora = tempo.substring(0, 2);
  minuto = tempo.substring(3, 5);


  
  pinMode(CS_PIN, OUTPUT);
  

  lcd.setCursor(0, 0);
  lcd.print("Iniciando Sistema");

  lcd.setCursor(3, 1);
  lcd.print("Bem-vindo");

  t1 = dht1.readTemperature();
  u1 = dht1.readHumidity();

    // Leitura do sensor 2
  t2 = dht2.readTemperature();
  u2 = dht2.readHumidity();

  tMedia = (t1 + t2) / 2;
  uMedia = (u1 + u2) / 2;

  lcd.clear();

  while(!SD.begin(CS_PIN))
  {
       
    lcd.setCursor(2, 0);
    lcd.print("Nao inciado");
    lcd.setCursor(0, 1);
    lcd.print("Insira o cartao");
    delay(1000);
    lcd.clear();
    delay(500);
    
  }
  
  lcd.clear();
  
  while(!SD.exists("produto.txt"))
  {
       
    lcd.setCursor(2, 0);
    lcd.print("Produto nao");
    lcd.setCursor(2, 1);
    lcd.print("selecionado");
    delay(1000);
    lcd.clear();
    delay(500);
    
  }

  lcd.clear();
  

  //Estrutura para verificar caso foi reiniciado o sistema
  
  if (SD.exists("STATUS.txt")) //Se existe STATUS, ele continua de onde parou
  {
    mStatus = leMStatus();//Recupera o ultimo minuto salvo no status
    hStatus = leHStatus();//Recupera a ultima hora salva no status
    dStatus = leDStatus();//Recupera o ultimo dia salvo no status

    passaDia(dStatus); //Define o dia atual com base no recebido
     
    salvaStatus(dStatus, hStatus, mStatus);//Salva o status atual usando a hora e o dia recuperados.

    salvaLog(data, tempo, tMedia, dAtivo[1], uMedia, dAtivo[2], dAtivo[0]);
  }
  else  // Se nao existe, ele começa um novo processo de maturação
  {
    
    dStatus = 1;
    salvaStatus(1, 0, 0);

    passaDia(dStatus);

    iniciaLog();
    salvaLog(data, tempo, tMedia, dAtivo[1], uMedia, dAtivo[2], dAtivo[0]);
  }

}



void loop()
{


  tempo = rtc.getTimeStr();
  data = rtc.getDateStr(FORMAT_LONG);

  hora = tempo.substring(0, 2);
  minuto = tempo.substring(3, 5);

  // Leitura do sensor 1
  t1 = dht1.readTemperature();
  u1 = dht1.readHumidity();

  // Leitura do sensor 2
  t2 = dht2.readTemperature();
  u2 = dht2.readHumidity();

  tMedia = (t1 + t2) / 2;
  uMedia = (u1 + u2) / 2;


  if ((millis() - cMillis) >= 10000) //Compara temperatura/umidade a cada 10s
  {
    comparaTemp(dAtivo[1], t1, t2);
    comparaUmi(dAtivo[2], u1, u2);
    cMillis = millis();
  }

  if ((millis() - sMillis) >= 60000) //Salva status a cada 1 min
  {
    hStatus = leHStatus();
    dStatus = leDStatus();
    mStatus = leMStatus();
    
    mStatus++; //Variavel contadora de minutos
    if (mStatus == 60)
    {
      mStatus = 0;
      hStatus++;
    }
    if (hStatus == 24)
    {
      hStatus = 0;
      dStatus++;
      passaDia(dStatus);
    }

    salvaStatus(dStatus, hStatus, mStatus);
    sMillis = millis();
  }

  if ((millis() - bMillis) >= 3600000) //Salva no BD a cada hora
  {
    salvaLog(data, tempo, tMedia, dAtivo[1], uMedia, dAtivo[2], dAtivo[0]);
    bMillis = millis();
  }

  if(fimMat)
  {
    finalizaLog(data, tempo, tMedia, dAtivo[1], uMedia, dAtivo[2], dAtivo[0], totalDias);
  }


  while(fimMat) //Verifica se a maturação foi encerrada e printa a mensagem para o usuario
  {
    
    lcd.setCursor(0, 0);   //Mostra data e hora
    lcd.print("    Fim da     ");    
    lcd.setCursor(0, 1);
    lcd.print("   Maturacao    ");
    delay(1000);
    lcd.clear();
    delay(500);
    
    
  }

int s = (tempo.substring(6, 8)).toInt(); //Atribui o valor em segundos para a variavel S

  if((s < 15) || (s > 30 && s < 45))
  {
    lcd.setCursor(1, 0);   //Mostra data e hora
    
    lcd.print("   Dia: ");
    lcd.print(dStatus);
    lcd.print("       ");
    
    lcd.setCursor(0, 1);
    lcd.print("T ");
    lcd.print(tMedia);
    lcd.write((byte)0);
    lcd.print("  ");
    lcd.print(dAtivo[1]);
    lcd.write((byte)0);
    
  }
  else
  {
    lcd.setCursor(1, 0);   //Mostra data e hora
    lcd.print(hora);
    lcd.print(":");
    lcd.print(minuto);
    lcd.print(" ");
    data = rtc.getDateStr(FORMAT_SHORT);   
    lcd.print(data);

  
    lcd.setCursor(0, 1);
    lcd.print("U ");
    lcd.print(uMedia);
    lcd.write("%");
    lcd.print("  ");
    lcd.print(dAtivo[2]);
    lcd.write("%");

    
  }

}
