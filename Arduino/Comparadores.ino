
void comparaTemp(float ideal, float t1, float t2)
{
  float tMedia = (t1 + t2) / 2;
  float dif = t1 - t2;

  if (abs(dif) >= 1)
  {
    ligaCooler(); //liga a ventilaçao quando os sensores medirem temperaturas diferentes
  }
  else
  {
    desligaCooler();//desliga a ventilaçao quando os sensores medirem temperaturas diferentes
  }

  dif = ideal - tMedia;

  if (abs(dif) > 0.70 &&  tMedia > ideal ) // se t media > q a ideal ele liga a geladeira
  {
    ligaGeladeira();
  }
  else
  {
    desligaGeladeira();
  }

}
void comparaUmi(float ideal, float u1, float u2)
{
  float uMedia = (u1 + u2) / 2;
  float dif = abs(u1 - u2);

  if (dif >= 1)
  {
    ligaCooler(); //liga a ventilaçao quando os sensores medirem umidades diferentes
  }
  else
  {
    desligaCooler(); //desliga a ventilaçao quando os sensores medirem umidades diferentes
  }

  dif = abs(ideal - uMedia);


  if (dif > 0.70 && uMedia < ideal)
  {
    ligaUmidificador();
  }
  else
  {
    desligaUmidificador();
  }

}
