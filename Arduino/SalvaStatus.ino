void salvaStatus(int dStatus, int hStatus, int mStatus)
{

  SD.remove ("STATUS.txt");

  File arquivo = SD.open("STATUS.txt", FILE_WRITE);

  if (arquivo)
  {
    // Escreve [dia,tempo ligado]
    arquivo.print("{\"Dia\":");//Inicia um dia no log
    arquivo.print(dStatus);
    arquivo.print(",\"Hora\":" );
    arquivo.print(hStatus);
    arquivo.print(",\"Minuto\":" );
    arquivo.print(mStatus);
    arquivo.println("};");//Finaliza o dia no log

    arquivo.close();//Salva os dados no txt e fecha o arquivo
  }
  else
  {
    Serial.println("Falha ao abrir o arquivo STATUS!");
  }
  arquivo.close();
}
