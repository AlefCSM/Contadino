
void iniciaLog()
{
  char ant;
  String json2;
  
  File prod = SD.open("produto.txt");
  if(prod)
  {
    while(prod.available())
    {
      char ch = prod.read();
      if(ch ==',' && ant == '}')
      {                
          StaticJsonBuffer<200> jsonBuffer2;
          JsonObject& root2 = jsonBuffer2.parseObject(json2);
          if(!root2.success())
          {
            Serial.println("parseObject() falhou");
            return;
          }
          String id = root2["id_historico"];

          Serial.println(json2);
          
          File arquivo = SD.open("LOG.txt", FILE_WRITE);
          
          arquivo.print("{\"id_historico\":\"");//Inicia um dia no log
          arquivo.print(id);
          arquivo.println("\",\"dados\":[");

          arquivo.close();//Salva os dados no txt e fecha o arquivo
          prod.close();
          return;
      }
      else
      {
        json2.concat(ch);
      }
      ant = ch;
    }
  }
}

void salvaLog(String data, String hora,  float temp,float tempEsp, float umi, float umiEsp, int dAtual )
{

  String diaLog,mesLog,anoLog;
  
  diaLog = data.substring(0,2);
  mesLog = data.substring(3,5);
  anoLog = data.substring(6,10);
  



  
  File arquivo = SD.open("LOG.txt", FILE_WRITE);

  if (arquivo)
  {
    arquivo.print("{\"data\":\"");//Inicia um dia no log
    arquivo.print(anoLog);
    arquivo.print("-");
    arquivo.print(mesLog);
    arquivo.print("-");
    arquivo.print(diaLog);
    arquivo.print(" ");
    arquivo.print(hora);
    arquivo.print("\",\"temperatura\":\"");
    arquivo.print(temp);
    arquivo.print("\",\"temperatura_esperada\":\"");
    arquivo.print(tempEsp);
    arquivo.print("\",\"umidade\":\"");
    arquivo.print(umi);
    arquivo.print("\",\"umidade_esperada\":\"");
    arquivo.print(umiEsp);
    arquivo.print("\",\"dia_atual\":\"");
    arquivo.print(dAtual);
    arquivo.println("\"},");//Finaliza o dia no log

    arquivo.close();//Salva os dados no txt e fecha o arquivo

  }

  else
  {
    Serial.println("Falha ao abrir o arquivo LOG! ");
  }
}

void finalizaLog(String data, String hora,  float temp,float tempEsp, float umi, float umiEsp, int dAtual, int dTotal)
{
  
  String diaLog,mesLog,anoLog;
  
  diaLog = data.substring(0,2);
  mesLog = data.substring(3,5);
  anoLog = data.substring(6,10);
  

  File arquivo = SD.open("LOG.txt", FILE_WRITE);

  if (arquivo)
  {
    arquivo.print("{\"data\":\"");//Inicia um dia no log
    arquivo.print(anoLog);
    arquivo.print("-");
    arquivo.print(mesLog);
    arquivo.print("-");
    arquivo.print(diaLog);
    arquivo.print(" ");
    arquivo.print(hora);
    arquivo.print("\",\"temperatura\":\"");
    arquivo.print(temp);
    arquivo.print("\",\"temperatura_esperada\":\"");
    arquivo.print(tempEsp);
    arquivo.print("\",\"umidade\":\"");
    arquivo.print(umi);
    arquivo.print("\",\"umidade_esperada\":\"");
    arquivo.print(umiEsp);
    arquivo.print("\",\"dia_atual\":\"");
    arquivo.print(dAtual);
    arquivo.print("\"}],\"total_dias\":\"");
    arquivo.print(dTotal);
    arquivo.println("\"}");//Finaliza o dia no log

    arquivo.close();//Salva os dados no txt e fecha o arquivo

  }
 

}
