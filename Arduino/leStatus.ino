int leDStatus() //Realiza a leitura do Dia do Status
{
  File stat = SD.open("STATUS.txt");

  char c;
  String d;
  int di;

  if (stat)
  {
    while (stat.available()) //enquanto o arquivo nao terminar
    {
      c = stat.read();

      if (c == ';') // se for [ é pq iniciou o arquivo
      {
        StaticJsonBuffer<200> jsonBuffer3;
        JsonObject& root3 = jsonBuffer3.parseObject(d);
        if(!root3.success())
        {
          Serial.println("parseObject() falhou");
          return;
        }

        di = root3["Dia"];  
      }
      else
      {
        d.concat(c);
      }
    }
    stat.close();
  }
  
  return di;
}

int leHStatus()
{

  File stat = SD.open("STATUS.txt");

  char c;
  String h;
  int ho;

  if (stat)
  {
    while (stat.available()) //enquanto o arquivo nao terminar
    {
      c = stat.read();

      if (c == ';') // se for [ é pq iniciou o arquivo
      {
        StaticJsonBuffer<200> jsonBuffer3;
        JsonObject& root3 = jsonBuffer3.parseObject(h);
        if(!root3.success())
        {
          Serial.println("parseObject() falhou");
          return;
        }

        ho = root3["Hora"];
      }
      else
      {
        h.concat(c);
      }
    }
    stat.close();
  }
  
  return ho;

}

int leMStatus()
{

  File stat = SD.open("STATUS.txt");

  char c;
  String m;
  int mi;

  if (stat)
  {
    while (stat.available()) //enquanto o arquivo nao terminar
    {
      c = stat.read();

      if (c == ';') // se for [ é pq iniciou o arquivo
      {
        StaticJsonBuffer<200> jsonBuffer3;
        JsonObject& root3 = jsonBuffer3.parseObject(m);
        if(!root3.success())
        {
          Serial.println("parseObject() falhou");
          return;
        }

        mi = root3["Minuto"];
      }
      else
      {
        m.concat(c);
      }
    }
    stat.close();
  }
  
  return mi;

}
