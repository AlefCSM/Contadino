
void ligaCooler()
{
  circulacao = true;
  digitalWrite(cooler, LOW);
}

void desligaCooler()
{
  circulacao = false;
  digitalWrite(cooler, HIGH);
}

void ligaGeladeira()
{
  digitalWrite(geladeira, LOW);
}
void desligaGeladeira()
{
  digitalWrite(geladeira, HIGH);
}
void ligaUmidificador()
{
  digitalWrite(umidificador, LOW);
}
void desligaUmidificador()
{
  digitalWrite(umidificador, HIGH);
}
