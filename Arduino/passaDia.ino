void passaDia(int d)
{

  int contDia = -1;
  char ant;
  String json;
  StaticJsonBuffer<200> jsonBuffer;

  File prod = SD.open("produto.txt");
  if(prod)
  {
    while(prod.available())
    {
      char ch = prod.read();
      if((ch ==',' && ant == '}') || ch == ';')
      {
        contDia++;
        if(contDia == d)
        {
          JsonObject& root = jsonBuffer.parseObject(json);
          if(!root.success())
          {
            Serial.println("parseObject() falhou");
            return;
          }
          float temp = root["temperatura"];          
          float umi = root["umidade"];

          dAtivo[0] = d;
          dAtivo[1] = temp;
          dAtivo[2] = umi;
          return;
        }
        else
        {
          json = "";
        }

       
      }
      else
      {
        json.concat(ch);
      }
      ant = ch;
    }

    if(json == "")
    {
          fimMat = true;
          totalDias = contDia;
          return;
    }
        
  }
  else
  {
    Serial.println("Erro ao abrir o arquivo");
  }
  prod.close();
  contDia = 0;
}
