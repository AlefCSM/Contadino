const app = require('./config/custom-express')();
const browserSync = require('browser-sync');
const server = require("http").Server(app);
const io = require('socket.io')(server);
const PORT = process.env.PORT || 3000;

app.set('io', io);

server.listen(PORT, () => {
    console.log('Servidor rodando');
    // descomentar quando estiver em desenvolvimento,
    // browserSync serve para recarregar páginas automaticamente, facilitando a vida do programador
    browserSync({
        files: ['src/**/*.{ejs,js,css}'],
        online: false,
        open: false,
        port: PORT+1,
        proxy: 'localhost:' + PORT,
        ui: false
    });
});

