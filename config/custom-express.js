const express = require("express");
const consign = require("consign");
const bodyParser = require("body-parser");
const expressValidator = require("express-validator");
const session = require("express-session");
const cors = require("cors");
const moment = require("moment");

// var morgan = require('morgan');
// var logger = require('../src/models/logger.js');

module.exports = function(){
    const app = express();

    // app.use(morgan("common", {
    //     stream: {
    //         write: mensagem => {
    //             logger.log('silly',mensagem);
    //         }
    //     }
    // }));
    app.use(bodyParser.urlencoded({extended:true}));
    app.use(express.static("./src/assets"));
    app.use(bodyParser.json());
    app.use(expressValidator());
    app.use(session({secret:'bananas',resave: false, saveUninitialized: false}));
    app.use(cors());

    app.set("view engine", "ejs");
    app.set("views","./src/views");
    app.set("moment",moment);

    consign()
        .include("src/controllers")
        .then("src/models")
        .into(app);

    app.use(function(req, res, next){
        res.status(404).render("error-pages/page-error-400");
    });

    app.use(function(error,req, res, next){
        console.info(error);
        res.status(500).render("error-pages/page-error-500",error);
    });

    return app;
};