function ProdutosDAO(connection) {
    this._connection = connection;
}

ProdutosDAO.prototype.add = async function (produto) {

    produto.status = false;
    produto.ciclos = [{qtd_dias: "", temperatura: "", umidade: ""}];
    let produtoRef = await this._connection
        .collection('produtos')
        .add(produto)
        .then(doc => {
            return doc.id;
        });
    return produtoRef;
};
//
ProdutosDAO.prototype.findAll = async function () {
    return await this._connection
        .collection('produtos')
        .get()
        .then(snapshot => {
            let produtos = [];
            snapshot.forEach(doc => {
                let produto = doc.data();
                produto.id = doc.id;
                produtos.push(produto);
            });
            return produtos;
        })
        .catch(err => {
            console.log('Error getting documents', err);
        });
};


ProdutosDAO.prototype.findById = async function (id) {
    let produtoDoc = await this._connection
        .collection('produtos')
        .doc(id)
        .get()
        .then(doc => {
            if (!doc.exists) {
                console.log('Produto não encontrado!');
                return null;
            } else {
                console.log('Document data:', doc.data());
                return doc.data();

            }
        })
        .catch(err => {
            console.log('Error getting document', err);
        });
    return produtoDoc;
};

ProdutosDAO.prototype.findAtivo = async function () {
    let produtoDoc = await this._connection
        .collection("produtos")
        .where("status", "==", true)
        .get()
        .then(documents => {
            let ativo = false;
            documents.forEach(doc => {
                ativo = true;
            });

            return ativo;
        })
        .catch(err => {
            console.log('Error getting document', err);
        });
    return produtoDoc;
};

ProdutosDAO.prototype.edit = async function (id, produto) {
    let batch = await this._connection.batch();

    console.info(id);

    let produtoRef = await this._connection
        .collection('produtos')
        .doc(id);

    batch.update(produtoRef, produto);

    let histRef = await this._connection
        .collection('historico')
        .where("referencia", "==", id);
    let histDocRef = await this._connection
        .collection('historico');

    histRef
        .get()
        .then(async documentos => {
            await documentos.forEach(async doc => {
                let finalRef = await histDocRef.doc(doc.id);
                batch.update(finalRef, {'nome_produto': produto.nome})
            });
            batch.commit();
        });

    return produtoRef;
};

ProdutosDAO.prototype.editCiclos = async function (id, ciclos) {
    let produtoDoc = await this._connection
        .collection('produtos')
        .doc(id)
        .update({ciclos: ciclos})
        .then(doc => {
            return true;
        })
        .catch(err => {
            console.log('Error getting document', err);
            return err;
        });
    console.info('prod => ' + produtoDoc);
    return produtoDoc;
};

ProdutosDAO.prototype.delete = async function (id) {
    let produtoDoc = await this._connection
        .collection('produtos')
        .doc(id)
        .delete();

    console.info('prod => ' + produtoDoc)
    return produtoDoc;
};

module.exports = () => {
    return ProdutosDAO;
};