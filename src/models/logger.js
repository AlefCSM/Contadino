var winston = require('winston');
var fs = require('fs');

if(!fs.existsSync('logs')){
    fs.mkdirSync('logs');
}

module.exports = winston.createLogger({
    transports: [
        new winston.transports.File({
            level: "silly",
            filename: "logs/system.log",
            maxsize: 100000,
            maxFiles: 10
        }),
        new winston.transports.File({
            level: "info",
            filename: "logs/info.log",
            maxsize: 100000,
            maxFiles: 10
        }),
        new winston.transports.File({
            level: "error",
            filename: "logs/error.log",
            maxsize: 1000000,
            maxFiles: 10
        })
    ],
    exceptionHandlers: [
        new winston.transports.File({
            level:"exception",
            filename: 'logs/exceptions.log' })
    ]
});