function LoginDAO(connection) {
    this._connection = connection;
}

LoginDAO.prototype.registrar = async function (registro)  {

    this._connection
        .collection('usuarios')
        .doc(registro.id)
        .set({
            nome: registro.nome,
            email: registro.email
        })
        .then(doc => {
            console.log(doc.id);
            return doc.id;
        });
};

LoginDAO.prototype.findById = async function (id) {
    let usuarioDoc = await this._connection.collection('usuarios').doc(id).get()
        .then(doc => {
            if (!doc.exists) {
                console.log('Documento não encontrado!');
                return null;
            } else {
                console.log('Document data:', doc.data());
                return doc.data();
            }
        })
        .catch(err => {
            console.log('Error getting document', err);
        });
    return usuarioDoc;
};

module.exports = () => {
    return LoginDAO;
};