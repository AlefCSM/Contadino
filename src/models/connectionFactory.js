const admin = require("firebase-admin");
require("firebase/firestore");
const serviceAccount = require("../../firebase-config.json");



function firebaseDBConnection() {
    if (!admin.apps.length) {
        admin.initializeApp({
            credential: admin.credential.cert(serviceAccount),
            databaseURL: "https://teste-hosting-3ea7e.firebaseio.com"
        });
    }else{
        admin.app();
    }
    return  admin.firestore();
}

module.exports = {
    firebaseCon: function(){
        return firebaseDBConnection();
    }
};