function HistoricosDAO(connection) {
    this._connection = connection;
}

HistoricosDAO.prototype.addHistorico = async function (referencia) {

    let historico = {};

    historico.referencia = referencia;
    historico.data_inicio = Math.round(new Date().getTime() / 1000).toString();
    historico.data_fim = "";

    let produtoRef = await this._connection
        .collection('historico')
        .add(historico)
        .then(doc => {
            return doc.id;
        });
    return produtoRef;
};

HistoricosDAO.prototype.editHistorico = async function (id,historico) {

    let historicoRef = await this._connection
        .collection("historico")
        .doc(id)
        .update(historico)
        .then(doc => {
            let historico = doc
            historico.id = doc.id;
            return historico;
        })
        .catch(err => {
            console.log('Error getting documents', err);
        });

    return historicoRef;
};

HistoricosDAO.prototype.findLastHistorico = async function () {
    let historicoRef = await this._connection
        .collection("historico")
        .orderBy("data_inicio", "desc")
        .limit(1)
        .get()
        .then(snapshot => {
            let historico = {};
            snapshot.forEach(doc => {
                historico = doc.data();
                historico.id = doc.id;
            });
            return historico;
        })
        .catch(err => {
            console.log('Error getting documents', err);
        });

    return historicoRef;
};

HistoricosDAO.prototype.findAllHistoricos = async function () {
    let historicoRef = await this._connection
        .collection("historico")
        .get()
        .then(documentos => {
            let historicos = [];
            let historico = {};
            documentos.forEach(doc => {
                historico = doc.data();
                historico.id = doc.id;
                historicos.push(historico);
            });
            return historicos;
        })
        .catch(err => {
            console.log('Error getting documents', err);
        });

    return historicoRef;
};

HistoricosDAO.prototype.findHistoricoById = async function (id) {
    let historicoRef = await this._connection
        .collection("historico")
        .doc(id)
        .get()
        .then(doc => {
            let historico = doc.data();
            historico.id = doc.id;
            return historico;
        })
        .catch(err => {
            console.log('Error getting documents', err);
        });

    return historicoRef;
};

HistoricosDAO.prototype.findHistoricosByProduto = async function (prodId) {
    let historicoRef = await this._connection
        .collection("historico")
        .where("referencia","==",prodId)
        .get()
        .then(documentos => {
            let historicos = [];
            let historico = {};
            documentos.forEach(doc => {
                historico = doc.data();
                historico.id = doc.id;
                historicos.push(historico);
            });
            return historicos;
        })
        .catch(err => {
            console.log('Error getting documents', err);
        });

    return historicoRef;
};

HistoricosDAO.prototype.findAllDados = async function (id) {
    let historicoRef = await this._connection
        .collection("historico")
        .doc(id)
        .collection('dados')
        .orderBy("data", "asc")
        .get()
        .then(documentos => {
            let grafico = [];
            let historico = {};
            documentos.forEach(doc => {
                historico = doc.data();
                historico.id = doc.id;
                grafico.push(historico);
            });
            return grafico;
        })
        .catch(err => {
            console.log('Error getting documents', err);
            return null;
        });

    return historicoRef;
};

HistoricosDAO.prototype.findLastDados = async function (id) {
    let dadosRef = await this._connection
        .collection("historico")
        .doc(id)
        .collection('dados')
        .orderBy("data", "desc")
        .limit(1)
        .get()
        .then(snapshot => {
            let dados;
            snapshot.forEach(doc => {
                dados = doc.data();
                dados.id = doc.id;
                return dados;
            });
            return dados;
        })
        .catch(err => {
            console.log('Error getting documents', err);
        });

    return dadosRef;
};

HistoricosDAO.prototype.addDados = async function (id, dados) {
    await this._connection
        .collection('historico')
        .doc(id)
        .collection('dados')
        .add(dados);
};
//
module.exports = () => {
    return HistoricosDAO;
};