firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
        // User is signed in.
        let email = user.email;
        let uid = user.uid;
        let nome = $('#nome').val();

        $.ajax({
            url: '/registrar',
            type: 'post',
            data: {
                nome: nome,
                email: email,
                id: uid
            },
            succesful: data => {

            },
            error: error => {
                console.log(error);
                alert('Não foi possível concluir o registro');
            }
        });
    } else {
        // User is signed out.
        // ...
    }
});


function registrar() {
    let email = $('#email').val();
    let senha = $('#senha').val();

    firebase.auth().createUserWithEmailAndPassword(email, senha)
        .then(()=>{
            location.href = '/login';
        })
        .catch(function (error) {
            console.log(error);
            alert(error);
        });
}