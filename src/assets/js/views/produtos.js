function addCiclo() {
    let index = ciclos.length;
    let i = index + 1;
    $('#marotinho').append(
        '<div class="col-lg-12 row" id="ciclo' + index + '">\n' +
        '     <div class="col-lg-12">\n' +
        '          <h4>Etapa ' + i + '</h4>\n' +
        '     </div>\n' +
        '     <div class="form-group col-lg-3">\n' +
        '            <label for="nome">Quantidade de dias<span class="text-danger">*</span></label> \n' +
        '            <input type="number" class="form-control add_dias" id="qtd_dias' + index + '"\n' +
        '                        name="qtd_dias' + index + '" placeholder="Digite uma quantidade" min="1" value="1"/>\n' +
        '     </div>\n' +
        '     <div class="form-group col-lg-3">\n' +
        '            <label for="nome">Temperatura(°C)<span class="text-danger">*</span></label>\n' +
        '            <input type="number" class="form-control" id="temperatura' + index + '"\n' +
        '                        name="temperatura' + index + '" placeholder="Digite uma temperatura" value=""/>\n' +
        '     </div>\n' +
        '     <div class="form-group col-lg-3">\n' +
        '            <label for="nome">Umidade(%UR)<span class="text-danger">*</span></label>\n' +
        '            <input type="number" class="form-control" id="umidade' + index + '"\n' +
        '                        name="umidade' + index + '" placeholder="Digite uma quantidade" value=""/>\n' +
        '     </div>\n' +
        '     <div class="form-group ">\n' +
        '          <button class="btn btn-youtube" onclick="delCiclo(' + index + ')" style="margin-top: 35px;">Excluir</button>\n' +
        '     </div>\n' +
        '</div>'
    );
    qtd++;
    ciclos.push(1);
}

function delCiclo(index) {
    $('#ciclo' + index).remove();
    updateIndex();
    ciclos.pop(index);
    qtd_total_dias();
}

function updateIndex() {
    ciclos.forEach((ciclo, index) => {
        let etapa = index + 1;
        $('#marotinho div:nth-child(' + etapa + ')').children('div').children('h4').text('Etapa ' + etapa);
    });
}

function deletar(id) {
    $('#form_produto')
        .removeAttr("action")
        .attr("action", "/produtos/" + id + "/deletar")
        .submit();
};

function novaMaturacao(id) {

    let a = document.createElement('a');
    a.href = '/api/getProduto/' + id;
    a.download = 'produto.txt';
    a.click();
    window.location.href = "/";
};

function saveCiclo(id) {
    let salvar_ciclos = [];
    let erros = false;
    ciclos.forEach((ciclo, index) => {
        let i = {};
        let etapa = index + 1;
        i.qtd_dias = $('#marotinho div:nth-child(' + etapa + ')').children('div:nth-child(2)').children('input[id*="qtd_dias"]').val();
        i.temperatura = $('#marotinho div:nth-child(' + etapa + ')').children('div:nth-child(3)').children('input[id*="temperatura"]').val();
        i.umidade = $('#marotinho div:nth-child(' + etapa + ')').children('div:nth-child(4)').children('input[id*="umidade"]').val();

        if (i.umidade == "" || i.temperatura == "" || i.qtd_dias == "") {
            erros = true
        }
        salvar_ciclos.push(i);
    });
    if (erros) {
        alert("foram encontrados compos nulos no cadastro!");
        return;
    }
    $.ajax({
        url: '/produtos/ciclos/' + id,
        type: 'post',
        data: {salvar_ciclos},
        succesful: data => {
            alert(data);
        },
        error: error => {
            console.info(error);
            alert('Não foi possível salvar os ciclos');
        }
    });
}

function qtd_total_dias() {
    let dias = [];
    $('input[name*="qtd_dias"]').each(function () {
        dias.push(parseInt($(this).val() === '' ? 0 : $(this).val()));
    });
    let qtd = dias.reduce((a, b) => a + b, 0);
    $('#cont_dias').text(qtd);
}

$(document).on('keyup', 'input', '.add_dias', (e) => {
    e.stopPropagation();
    qtd_total_dias();
});

$(document).ready(function () {
    qtd_total_dias();
});

function verDetalhes(id) {
    window.location.href = "/produtos/" + id;
}

//var socket = io.connect('htp://localhost:3001');

