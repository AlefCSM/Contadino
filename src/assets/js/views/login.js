function login() {
    let email = $('#email').val();
    let senha = $('#senha').val();

    firebase.auth().signInWithEmailAndPassword(email, senha)
        .then(() => {
            let usuario = firebase.auth().currentUser;

            $.ajax({
                url: '/login',
                type: 'post',
                data: {
                    id: usuario.uid
                },
                error: error => {
                    console.log(error);
                    alert('Não foi possível efetuar o login');
                }
            })
                .done(data => {
                    location.href = '/index';
                });
        })
        .catch(function (error) {
            console.log(error);
            alert(error);
        });
}

function logout(){
    firebase.auth().signOut().then(function() {
        console.info("logout talkei");
        location.href = '/login';
    }).catch(function(error) {
        // An error happened.
    });
}