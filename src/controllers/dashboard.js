const socket = require('socket.io');

module.exports = function (app) {

    app.get(['/', '/index'], checkSession, async (req, res) => {

        let connection = new app.src.models.connectionFactory.firebaseCon();
        let historicosDAO = new app.src.models.HistoricosDAO(connection);
        let produtosDAO = new app.src.models.ProdutosDAO(connection);

        let historico = await historicosDAO.findLastHistorico();
        historico.dados = await historicosDAO.findLastDados(historico.id);
        let dadosGraficos = await historicosDAO.findAllDados(historico.id);
        let produto = await produtosDAO.findById(historico.referencia);

        console.info('grafico');
        console.info(dadosGraficos);

        let relatorio = {};
        relatorio.grafico = {};
        relatorio.grafico.temperatura = [];
        relatorio.grafico.umidade = [];
        relatorio.grafico.label = [];
        dadosGraficos.forEach(dado => {
            relatorio.grafico.temperatura.push(dado.temperatura);
            relatorio.grafico.umidade.push(dado.umidade);
            relatorio.grafico.label.push(app.get("moment").unix(dado.data).format('DD/MM/YYYY HH:MM'));
        });

        relatorio.produto_nome = produto !== null ? produto.nome : null;
        if (relatorio.grafico !== null && (historico.dados !== null && historico.dados !== undefined)) {
            console.info(historico);
            relatorio.dia_atual = historico.dados.dia_atual;
            relatorio.dia_total = historico.dados.total_dias;
            relatorio.temperatura_atual = historico.dados.temperatura;
            relatorio.temperatura_esperada = historico.dados.temperatura_esperada;
            relatorio.umidade_atual = historico.dados.umidade;
            relatorio.umidade_esperada = historico.dados.umidade_esperada;

        } else {
            relatorio.dia_atual = null;
            relatorio.dia_total = null;
            relatorio.temperatura_atual = null;
            relatorio.temperatura_esperada = null;
            relatorio.umidade_atual = null;
            relatorio.umidade_esperada = null;
        }
        //console.info(relatorio);
        res.set('Cache-Control', 'public, max-age=300, s-maxage=600');
        res.render('index.ejs', {
            title: 'Contadino',
            relatorio: relatorio
        });
    });


    app.get('/historicos', async (req, res) => {
        console.log('cadastro de produto');
        //logger.info('cadastro de produto');

        let connection = new app.src.models.connectionFactory.firebaseCon();
        let historicosDAO = new app.src.models.HistoricosDAO(connection);

        let historicos = await historicosDAO.findAllHistoricos();
        //let historicos = await historicosDAO.findAllHistoricos();
        let registros = [];

        const teste = async () => {
            for (const historico of historicos) {
                await historicosDAO
                    .findAllDados(historico.id)
                    .then(dados => {
                        for (let dado of dados) {
                            dado.id_historico = historico.id;
                            dado.nome_produto = historico.nome_produto;
                            registros.push(dado);
                        }
                    });
            }
        };

        await teste();
        console.info("registro vvvvv");
        console.info(registros);
        res.set('Cache-Control', 'public, max-age=300, s-maxage=600');
        res.render('historicos.ejs', {
            title: 'Contadino',
            registros: registros,
            page: 'Históricos',
            moment: app.get("moment")
        });
    });
};

function checkSession(req, res, next) {
    if (req.session['nome']) {
        next();
    }
    else {
        res.redirect('/login');
    }
};
