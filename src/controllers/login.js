module.exports = function(app) {

    app.get('/login', (req, res) => {
        req.session.destroy();
        res.render('login.ejs',{title:'Contadino'});
    });

    app.post('/login',async (req, res) => {


        let login = req.body;
        console.log(login);

        let connection = new app.src.models.connectionFactory.firebaseCon();
        let loginDAO = new app.src.models.LoginDAO(connection);

        let session = await loginDAO.findById(login.id);


        console.info(session);
        req.session['nome'] = session.nome;
        req.session['email'] = session.email;
        console.info(req.session);
        res.status(200).send(session);
    });

    app.get('/registrar', (req, res) => {
        res.render('registrar.ejs',{title:'Contadino'});
    });

    app.post('/registrar', async (req, res) => {

        let registro = req.body;

        let connection = new app.src.models.connectionFactory.firebaseCon();
        let loginDAO = new app.src.models.LoginDAO(connection);

        loginDAO.registrar(registro);
        res.status(200).send();
    });
};