module.exports = function (app) {

    app.get('/api', (req, res) => {
        var text = "hello world";


        res.setHeader('Content-type', "application/octet-stream");
        res.setHeader('Content-disposition', 'attachment; filename=file.txt');
        res.send(text);
    });

    app.get('/api/getProduto/:id/', async (req, res) => {
        //esse metodo é responsavel por buscar o produto ativo
        //obs: é necessário assegurar de que haja somente 1 produto ativo

        let id = req.params.id;

        let connection = new app.src.models.connectionFactory.firebaseCon();
        let historicosDAO = new app.src.models.HistoricosDAO(connection);
        let produtoDAO = new app.src.models.ProdutosDAO(connection);

        let produto = await produtoDAO.findById(id);

        produto.status = true;
        produtoDAO.edit(id, produto);

        let id_historico = await historicosDAO.addHistorico(id);


        let string_produto = '{"id_historico" : ' + id_historico + '}';

        produto.ciclos.forEach(doc => {
            for (let i = 0; i < doc.qtd_dias; i++) {
                string_produto += ',{"temperatura" : ' + doc.temperatura + ',';
                string_produto += '"umidade" : ' + doc.umidade + '}';
            }
        });
        string_produto += ';';

        data = {};
        data.label = ["0", "0", "0", "0", "0", "0"];
        data.temperatura = [0, 0, 0, 0, 0, 0];
        data.umidade = [0, 0, 0, 0, 0, 0];

        app.get("io").emit('produto', data);


        res.setHeader('Content-type', "application/octet-stream");
        res.setHeader('Content-disposition', 'attachment; filename=produto.txt');
        console.info(string_produto);
        res.send(string_produto);


    });

    app.post('/api/postHistorico', async (req, res) => {

        req.assert("id_historico", "Informe o id do histórico").notEmpty();

        let erros = req.validationErrors();
        if (erros) {
            console.log('Erros de validacao encontrados');
            res.status(400).send(erros);
            return;
        }

        let object = req.body;

        let id = object.id_historico;
        let dados = object.dados;
        let connection = new app.src.models.connectionFactory.firebaseCon();
        let historicosDAO = new app.src.models.HistoricosDAO(connection);
        let produtosDAO = new app.src.models.ProdutosDAO(connection);

        let info = [];

        dados.forEach(async dado => {
            dado.data = Date.parse(dado.data) / 1000;
            dado.total_dias = object.total_dias;
            await historicosDAO.addDados(id, dado);
        });


        if (object.dia_fim) {
            let historico = await historicosDAO.findHistoricoById(id);

            let produto = await produtosDAO.findById(historico.referencia);
            console.info(historico.data_fim);
            historico.data_fim = Date.parse(object.dia_fim) / 1000;
            console.info(historico.data_fim);
            await historicosDAO.editHistorico(id, historico);
            produto.status = false;
            await produtosDAO.edit(historico.referencia, produto);
        }

        dados.forEach(dado => {
            dado.data = app.get('moment').unix(dado.data).format('DD/MM/YYYY HH:MM');
            dado.total_dias = object.total_dias;
            info.push(dado);
        });

        app.get('io').emit('grafico', info);
        res.status(200).send('OK');
    });


};