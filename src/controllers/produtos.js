const logger = require('../models/logger.js');
const moment = require('moment');

module.exports = function (app) {

    app.get('/produtos', checkSession, async (req, res) => {
        console.log('cadastro de produto');
        logger.info('cadastro de produto');

        let connection = new app.src.models.connectionFactory.firebaseCon();
        let produtosDAO = new app.src.models.ProdutosDAO(connection);

        let produtos = await produtosDAO.findAll();

        console.info(produtos);

        res.render('produtos.ejs', {
            title: 'Contadino',
            page: 'Produtos',
            produtos: produtos
        });
    });

    app.get('/produtos/cadastro', checkSession, (req, res) => {
        console.log('cadastro de produto');
        logger.info('cadastro de produto');

        let ativa = null;
        res.render('cadastro_produto.ejs', {
            title: 'Contadino',
            page: 'Cadastrar produtos',
            produto: "",
            historicos: "",
            maturacao_ativa: ativa

        });
    });

    app.get('/produtos/:id', checkSession, async (req, res) => {
        let id = req.params.id;

        let connection = new app.src.models.connectionFactory.firebaseCon();
        let produtosDAO = new app.src.models.ProdutosDAO(connection);
        let historicosDAO = new app.src.models.HistoricosDAO(connection);

        let produto = await produtosDAO.findById(id);
        let historicos = await historicosDAO.findHistoricosByProduto(id);
        let ativa = await produtosDAO.findAtivo();

        if (produto == null) {
            res.status(404).render("error-pages/page-error-400.ejs");
        } else {
            produto.id = id;
            res.render('cadastro_produto.ejs', {
                title: 'Contadino',
                page: 'Editar produtos',
                produto: produto,
                historicos: historicos,
                moment: moment,
                maturacao_ativa: ativa
            });
        }
    });

    app.post('/produtos/cadastrar', async (req, res) => {
        console.log('vai cadastrar');
        req.assert("nome", "Nome é obrigatório").notEmpty();
        req.assert("descricao", "O valor máximo é de 700 caracteres").isLength({max: 700});

        let erros = req.validationErrors();
        if (erros) {
            res.status(400).send(erros);
            return;
        }

        let produto = req.body;

        let connection = app.src.models.connectionFactory.firebaseCon();
        let produtosDAO = new app.src.models.ProdutosDAO(connection);

        let result = await produtosDAO.add(produto);

        console.log('id cadastrado => ', result);
        logger.info('id cadastrado => ', result);
        res.redirect('/produtos/' + result);
    });

    app.post('/produtos/:id/editar', async (req, res) => {
        console.log('editando produto');
        logger.info('editando produto');
        req.assert("nome", "Nome é obrigatório").notEmpty();
        req.assert("descricao", "O valor máximo é de 700 caracteres").isLength({max: 700});

        let erros = req.validationErrors();
        if (erros) {
            console.log('Erros de validacao encontrados');
            res.status(400).send(erros);
            return;
        }

        let produto = req.body;
        let id = req.params.id;


        let connection = app.src.models.connectionFactory.firebaseCon();
        let produtosDAO = new app.src.models.ProdutosDAO(connection);

        await produtosDAO.edit(id, produto);
        let result = await produtosDAO.findById(id);
        if (result) {
            res.redirect('/produtos/' + id);
        } else {
            res.status(400).send(result);
            return;
        }
    });

    app.post('/produtos/ciclos/:id', async (req, res) => {
        console.log('editando etapas');
        logger.info('editando etapas');

        let ciclos = req.body.salvar_ciclos;
        let id = req.params.id;

        let connection = app.src.models.connectionFactory.firebaseCon();
        let produtosDAO = new app.src.models.ProdutosDAO(connection);

        let result = await produtosDAO.editCiclos(id, ciclos);
        if (result) {
            res.status(200).send(result);
        } else {
            res.status(400).send(result);
            return;
        }
    });

    app.post('/produtos/:id/deletar', (req, res) => {
        console.log('deletando produto');
        logger.info('deletando de produto');

        let id = req.params.id;
        let connection = app.src.models.connectionFactory.firebaseCon();
        let produtosDAO = new app.src.models.ProdutosDAO(connection);
        let result = produtosDAO.delete(id);
        console.log(result);

        res.redirect('/index');
    });

    app.get('/teste', (req, res) => {
        console.log('Requisição de pagamento');

        res.render('table-bootstrap.ejs', {title: 'Contadino', registros: null, page: 'Históricos'});
    });

};

function checkSession(req, res, next) {
    if (req.session['nome']) {
        next();
    }
    else {
        res.redirect('/login');
    }
};